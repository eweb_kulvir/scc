-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 04, 2020 at 12:34 PM
-- Server version: 10.3.18-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `a1pro_scc`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `Firstname` varchar(255) NOT NULL,
  `Lastname` varchar(255) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `EmailAddress` varchar(255) NOT NULL,
  `event_you_like` varchar(255) NOT NULL,
  `person_Firstname` varchar(255) NOT NULL,
  `person_lastname` varchar(255) NOT NULL,
  `person_Email` varchar(255) NOT NULL,
  `current_address` varchar(255) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `person_DOB` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `Firstname`, `Lastname`, `PhoneNumber`, `EmailAddress`, `event_you_like`, `person_Firstname`, `person_lastname`, `person_Email`, `current_address`, `street_address`, `state`, `postcode`, `person_DOB`, `detail`, `created_date`) VALUES
(1, 'Tetser', 'sdfsdf', '9899898989898', 'admin123@gmail.com', 'Anniversary', 'Demo', '', 'admin123@gmail.com', '', '', '', '', '2020-02-26', 'acsc sdfsdfdf sdsdfds sdsdfdf', '2020-03-03 05:02:18'),
(2, 'Tetser', 'sdfsdf', '9899898989898', 'admin123@gmail.com', 'Graduation', 'shubham', 'RANA', 'admin123@gmail.com', 'sdf', 'Washington Street, DC, DC', 'NY', '10001', '1996-02-07', 'This is a demo data sdgdgdg dfgdgdg fddfggdfgd dfgdfgdf dfgdfgdgdfg dfgdfg dfgdfgdfgf dfg.', '2020-03-03 05:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `ask_pastor_wale`
--

CREATE TABLE `ask_pastor_wale` (
  `id` int(11) NOT NULL,
  `Firstname` varchar(255) NOT NULL,
  `Lastname` varchar(255) NOT NULL,
  `Emailaddress` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `Prayerfor` varchar(255) NOT NULL,
  `SpecialRequests` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ask_pastor_wale`
--

INSERT INTO `ask_pastor_wale` (`id`, `Firstname`, `Lastname`, `Emailaddress`, `Phone`, `dob`, `Prayerfor`, `SpecialRequests`, `created_date`) VALUES
(1, 'Tetser', '', 'admin123@gmail.com', '9899898989898', '', 'Need A Job', 'sdfsdfsdf sdfsdfsdf', '2020-03-03 05:03:37'),
(2, 'Tetser', '', 'admin123@gmail.com', '9899898989898', '', 'Healing', 'asdasd asdasd asdadasd asdasd', '2020-03-03 05:03:37'),
(3, 'test12', '', 'testdemo1www98@gmail.com', '23434324324', '', 'Salvation', 'fdsgdfg', '2020-03-03 05:03:37'),
(4, 'jiop', '', 'admin7867@gmail.com', '3455456756', '', 'Healing', 'ghjhj', '2020-03-03 05:03:37'),
(5, 'zxcvbdzb', '', 'tester75741@gmail.com', '576575756', '', '', '', '2020-03-03 05:03:37'),
(6, 'yfdgd', '', 'tester75741@gmail.com', '9880808', '', 'Salvation', '', '2020-03-03 05:03:37'),
(7, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '9899898989898', '1997-07-17', 'Healing', 'This is dumyy request.', '2020-03-03 05:03:37');

-- --------------------------------------------------------

--
-- Table structure for table `believers_class`
--

CREATE TABLE `believers_class` (
  `id` int(11) NOT NULL,
  `BeliversFirstName` varchar(255) NOT NULL,
  `BeliversLastName` varchar(255) NOT NULL,
  `BeliversPhone` varchar(255) NOT NULL,
  `BeliversEmail` varchar(255) NOT NULL,
  `BeliversAdd` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `BeliversMsg` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `believers_class`
--

INSERT INTO `believers_class` (`id`, `BeliversFirstName`, `BeliversLastName`, `BeliversPhone`, `BeliversEmail`, `BeliversAdd`, `city`, `state`, `zip`, `BeliversMsg`, `created_date`) VALUES
(1, 'Tester', 'dg', 'tettetr12@test.in', '9898989898', 'dgd', '', '', '', 'sdfsdfsdf', '2020-03-03 05:06:41'),
(2, 'Tetser', 'sdfsdf', '9899898989898', 'admin123@gmail.com', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'asdsadasdasdsad', '2020-03-03 05:06:41'),
(3, 'Tetser', 'sdfsdf', '9899898989898', 'admin123@gmail.com', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'This is testing froms', '2020-03-03 05:06:41'),
(4, 'hhh', 'hhh', '3333333333', 'w@gmail.com', 'rrrrr', 'rrrr', 'OK', '34567', 'fff', '2020-03-03 05:06:41'),
(5, 'hhh', 'hhh', '3333333333', 'w@gmail.com', 'rrrrr', 'rrrr', 'OK', '34567', 'ggg', '2020-03-03 05:06:41'),
(6, 'Tetser', 'sdfsdf', '9899898989898', 'admin123@gmail.com', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'ssdfsdfsdfsdf', '2020-03-03 05:06:41'),
(7, 'Tetser', 'sdfsdf', '9899898989898', 'admin123@gmail.com', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'ghjghjghjghj', '2020-03-03 05:06:41'),
(8, 'Tetser', 'sdfsdf', '9899898989898', 'admin123@gmail.com', 'Washington Street, DC, DC', 'New York', 'NY', '10001', '', '2020-03-03 14:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `page_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `category_id`, `user_id`, `title`, `slug`, `body`, `page_image`, `created_at`) VALUES
(2, '9', 7, 'Shubham Blog', 'Shubham-Blog', 'asdas adasd assad asdasdasd', 'paypal.PNG', '2020-03-03 12:07:16'),
(3, '7', 7, 'Post Two', 'Post-Two', 'This is DUmmy', 'paypal.PNG', '2020-03-03 12:14:21');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `name`, `created_at`) VALUES
(6, 5, 'Technology', '2018-07-19 04:47:45'),
(7, 5, 'Programming', '2018-07-19 04:48:04'),
(8, 6, 'Teaching', '2018-07-19 04:57:48'),
(9, 6, 'Blogging', '2018-07-19 04:58:04'),
(12, 7, 'admin-cat', '2020-01-30 07:43:32'),
(13, 7, 'admin-test', '2020-01-30 07:43:49');

-- --------------------------------------------------------

--
-- Table structure for table `circle_registeration`
--

CREATE TABLE `circle_registeration` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `areacode` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `street_address2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `name_church` varchar(255) NOT NULL,
  `name_pastor` varchar(255) NOT NULL,
  `location_church` varchar(255) NOT NULL,
  `title_ministry` varchar(255) NOT NULL,
  `auxiliary_department` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` varchar(255) NOT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `phone`, `email`, `message`, `create_at`, `update_at`) VALUES
(16, 'welcome', '5498236546546', 'testdemo198@gmail.com', 'defdsf', '2020-02-28 13:37:49', '2020-02-28 13:37:49'),
(15, 'welcome', '5498236546546', 'testdemo198@gmail.com', 'dfdfgdfg', '2020-02-28 13:29:57', '2020-02-28 13:29:57'),
(14, 'welcome', '5498236546546', 'testdemo198@gmail.com', 'sdfdsfdf', '2020-02-28 13:29:07', '2020-02-28 13:29:07'),
(13, 'fghgfh', '34234234324', 'admin@gmail.com', 'gfhgfh', '2020-02-28 13:24:52', '2020-02-28 13:24:52'),
(12, 'welcome', '5498236546546', 'admin5@gmail.com', 'sdfdsf', '2020-02-28 13:23:17', '2020-02-28 13:23:17'),
(17, 'welcome', '5498236546546', 'testdemo198@gmail.com', 'lhknkljbkljghkljuigkljbvg', '2020-02-28 13:38:38', '2020-02-28 13:38:38'),
(18, 'welcome', '5498236546546', 'testdemo198@gmail.com', 'gfhgfh', '2020-02-28 13:40:26', '2020-02-28 13:40:26'),
(19, 'Guri', '5498236546546', 'testdemo198@gmail.com', 'test', '2020-02-28 13:56:25', '2020-02-28 13:56:25');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `ComplaintMessage` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `EmailAddress` varchar(255) NOT NULL,
  `DOB` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `Subject`, `ComplaintMessage`, `FirstName`, `LastName`, `EmailAddress`, `DOB`, `created_date`) VALUES
(1, 'testing purpose', 'sadfsdfaasdfasfd', 'Tetser', 'sdfsdf', 'admin123@gmail.com', '', '2020-03-03 05:09:06'),
(2, 'Salvation', 'This is a testing Feedback', 'Shubha,', 'Rana', 'test1developer198@gmail.com', '1998-02-04', '2020-03-03 05:09:06'),
(3, 'Family', 'This is a Demo Message.', 'Tetser', 'sdfsdf', 'admin123@gmail.com', '1996-02-07', '2020-03-03 05:09:06');

-- --------------------------------------------------------

--
-- Table structure for table `footerBottom`
--

CREATE TABLE `footerBottom` (
  `id` int(11) NOT NULL,
  `footerText` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footerBottom`
--

INSERT INTO `footerBottom` (`id`, `footerText`, `status`, `created_at`, `updated_at`) VALUES
(1, '© 2020 Copyright SCC', '1', '2020-02-20 13:09:55', '2020-02-20 07:39:55');

-- --------------------------------------------------------

--
-- Table structure for table `homeSliderImages`
--

CREATE TABLE `homeSliderImages` (
  `imageId` int(11) NOT NULL,
  `imageName` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1=>show,0=>hide',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homeSliderImages`
--

INSERT INTO `homeSliderImages` (`imageId`, `imageName`, `status`, `created_at`, `updated_at`) VALUES
(9, '1582177390img.jpg', '1', '2020-02-20 11:13:10', '2020-02-20 05:43:10'),
(2, '1582115147img.jpg', '0', '2020-02-19 17:55:47', '2020-02-19 12:25:47'),
(3, '1582115151img.jpg', '0', '2020-02-19 17:55:51', '2020-02-19 12:25:51'),
(6, '1582119430img.png', '1', '2020-02-19 19:07:10', '2020-02-19 13:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `Invite`
--

CREATE TABLE `Invite` (
  `id` int(11) NOT NULL,
  `InviteName` varchar(255) NOT NULL,
  `InviteLast` varchar(255) NOT NULL,
  `InviteEmail` varchar(255) NOT NULL,
  `InvitePhone` varchar(255) NOT NULL,
  `InvitorName` varchar(255) NOT NULL,
  `InvitorLast` varchar(255) NOT NULL,
  `InvitorDateTime` varchar(255) NOT NULL,
  `InvitorEmail` varchar(255) NOT NULL,
  `InvitorPhone` varchar(255) NOT NULL,
  `InvitorAddress` varchar(255) NOT NULL,
  `InvitorCity` varchar(255) NOT NULL,
  `InvitorState` varchar(255) NOT NULL,
  `InvitorZip` varchar(255) NOT NULL,
  `SpecialRequests` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Invite`
--

INSERT INTO `Invite` (`id`, `InviteName`, `InviteLast`, `InviteEmail`, `InvitePhone`, `InvitorName`, `InvitorLast`, `InvitorDateTime`, `InvitorEmail`, `InvitorPhone`, `InvitorAddress`, `InvitorCity`, `InvitorState`, `InvitorZip`, `SpecialRequests`, `created_date`) VALUES
(1, 'Tester', 'dg', 'Tango', 'CHarli', '2011-08-26T13:45', '2011-08-26', '2020-02', '2011-W33', '13:45:00', 'asdasd', 'asdasd', 'asdasd', 'asdasdasd', '', '2020-03-03 05:10:14'),
(2, 'Tetet', 'd,mfd,ms', 'sdfsd', 'sdfdsf', '2011-08-19T13:45:00', '2011-08-19', '2011-08', '2011-W33', '13:45:00', 'dsfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', '', '2020-03-03 05:10:14'),
(3, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '9899898989898', 'Tetser', 'sdfsdf', '2011-08-20T13:45', 'admin123@gmail.com', '+919899898989898', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'sdsadasdsa', '2020-03-03 05:10:14');

-- --------------------------------------------------------

--
-- Table structure for table `job_application`
--

CREATE TABLE `job_application` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `current_address` varchar(255) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `special_requests` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_application`
--

INSERT INTO `job_application` (`id`, `first_name`, `middle_name`, `last_name`, `current_address`, `street_address`, `city`, `state`, `postcode`, `special_requests`, `email_address`, `phone`, `date`, `created_date`) VALUES
(1, 'Tetser', 'sdf', 'sdfsdf', 'sdf', 'Washington Street, DC, DC', 'New York', 'New York', '10001', 'sdfsdfsdf', 'admin123@gmail.com', '9899898989898', '2011-08-19T13:45:00', '2020-03-03 05:11:13'),
(2, 'Tetser', 'sdf', 'sdfsdf', 'sdf', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'Photographer', 'admin420@gmail.com', '9899898989898', '1995-02-15', '2020-03-03 05:11:13'),
(3, 'Tetser', 'sdf', 'sdfsdf', 'sdf', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'Ushering ,Media & Technical ,Programs ,Protocol ,Church Care ,Choir ,Welcome & Guest ,Treasury ,Ministries ,Veritas ,ManTheGate ,SCC Couples', 'admin123@gmail.com', '9899898989898', '1995-03-09', '2020-03-03 13:40:46'),
(4, 'Tetser', 'sdf', 'sdfsdf', 'sdf', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'Ushering ,Media & Technical ,Programs ,Protocol', 'admin123@gmail.com', '9899898989898', '1992-02-05', '2020-03-03 13:42:35');

-- --------------------------------------------------------

--
-- Table structure for table `kids`
--

CREATE TABLE `kids` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `manthegate`
--

CREATE TABLE `manthegate` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `emailaddr` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `streetaddr` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `churcheName` varchar(255) NOT NULL,
  `radio-group` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manthegate`
--

INSERT INTO `manthegate` (`id`, `fname`, `lname`, `phone`, `emailaddr`, `address`, `streetaddr`, `city`, `zip_code`, `state`, `age`, `churcheName`, `radio-group`, `created_date`) VALUES
(1, 'Tetser', 'sdfsdf', '9899898989898', 'admin123@gmail.com', 'mohali', 'Washington Street, DC, DC', 'New York', '10001', 'NY', '1997-02-05', 'Tetser sdfsdf', 'on', '2020-03-03 05:15:09'),
(2, 'Tango', 'Charli', '6565986598', 'tangocha12@test.on', 'Mohali', 'CHD', 'Chad', '160055', 'AZ', '1999-02-18', 'Mohali ', 'Brotherhood Department', '2020-03-03 05:15:09'),
(3, 'Tetser', 'sdfsdf', '9899898989898', 'admin123@gmail.com', 'mohali', 'Washington Street, DC, DC', 'New York', '10001', 'NY', '1998-02-11', '', '', '2020-03-03 05:15:09'),
(4, 'Tester', 'Demo', '986521545', 'tesete12@test.in', 'Mohali', 'Mohali', 'Mojalu', '1236454', 'AL', '1980-09-18', '', '', '2020-03-03 05:15:09');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `Lastname` varchar(255) NOT NULL,
  `MemberEmail` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `MemberPhone` varchar(255) NOT NULL,
  `MemberAddress` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `MemberSex` varchar(255) NOT NULL,
  `MemberAge` varchar(255) NOT NULL,
  `MemberRole` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `Lastname`, `MemberEmail`, `username`, `MemberPhone`, `MemberAddress`, `zipcode`, `MemberSex`, `MemberAge`, `MemberRole`, `password`, `created_at`) VALUES
(4, 'AwebSolutionsTester', '', 'aweb12@demoeeeee.in', '', '2569222', 'HimachalPradesh', '', 'Male', '', '', '', '2020-03-03 05:37:25'),
(5, 'Demo2', '', 'demo22@test.in', '', '9854125487', 'mohali', '', 'Male', '', '', '', '2020-03-03 05:37:25'),
(6, 'Suraj', '', 'suraj12@test.in', '', '9854125487', 'Panchkula', '', 'Male', '', '', '', '2020-03-03 05:37:25'),
(7, 'Shubham', 'Rana', 'shubh12@test.in', 'shubham', '123654897898', 'Washington Street, DC, DC', '', 'Male', '25', 'Salvation', 'd41d8cd98f00b204e9800998ecf8427e', '2020-03-03 07:25:52'),
(12, 'Bunty', 'Rana', 'buntyrana12@test.in', 'buntyrana', '9856321548', 'Washington Street, DC, DC, DC', '10001', 'Male', '35', 'Salvation', 'd41d8cd98f00b204e9800998ecf8427e', '2020-03-03 10:12:18'),
(13, 'Shubham', 'Rana', 'shubhdemo13@test.in', 'shuhamrana', '9854125487', 'mohali', '123654', 'Male', '52', 'Salvation', 'd41d8cd98f00b204e9800998ecf8427e', '2020-03-03 10:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `office_scheduler`
--

CREATE TABLE `office_scheduler` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `special_requests` varchar(255) NOT NULL,
  `created_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `office_scheduler`
--

INSERT INTO `office_scheduler` (`id`, `first_name`, `last_name`, `phone`, `email`, `address`, `city`, `state`, `postcode`, `special_requests`, `created_date`) VALUES
(1, 'Tetser', 'sdfsdf', 2147483647, 'admin123@gmail.com', 'Mohali Punjab', 'New York', 'New York', '10001', 'qeqweqee', ''),
(10, 'Tetser', 'sdfsdf', 2147483647, 'admin123@gmail.com', 'mohali', 'New York', 'New York', '10001', 'asasasas', ''),
(9, 'Tetser', 'sdfsdf', 2147483647, 'admin123@gmail.com', 'mohali', 'New York', 'New York', '10001', 'asdas asdasfdasf', ''),
(11, 'Tetser', 'sdfsdf', 0, '9899898989898', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'fsdfs ssdfs sdf sdf sdfsfsdf setwethc ageh', ''),
(12, 'Tetser', 'sdfsdf', 0, '9899898989898', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'df dfgdf', ''),
(13, 'Tetser', 'sdfsdf', 2147483647, 'admin123@gmail.com', 'Washington Street, DC, DC', 'New York', 'NY', '10001', 'jhtuty iuyiuy ', '');

-- --------------------------------------------------------

--
-- Table structure for table `onlineVisitors`
--

CREATE TABLE `onlineVisitors` (
  `id` int(11) NOT NULL,
  `ip_address` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `visited_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `online_donations`
--

CREATE TABLE `online_donations` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `amount` int(255) NOT NULL,
  `donated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `page_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `category_id`, `user_id`, `title`, `slug`, `body`, `page_image`, `created_at`) VALUES
(1, '7', 7, 'Testing Page', 'Testing-Page', 'Schaumburg Community Church (S.C.C.) was birthed by the Chicago Parish of the Redeemed Christian Church of God in line with fulfilling its vision to plant churches around the world designed to encourage the propagation of the gospel of Jesus Christ and to provide a close place of worship, fellowship and discipleship for every convert.\r\n\r\nS.C.C. is led by Wale and FolukeAkinosun who have served at the Chicago Parish since 1995. The church aims to impact its local community by preaching the gospel and partnering with the village of Schaumburg to provide love, leadership, and development to its people.\r\n\r\nYou may reach Pastor Wale by email pastorwale@rccgschaumburg.org or 847-925-\r\n0000 To impact our local community for Christ by:\r\nPreaching the gospel to the community\r\nDiscipling the believers\r\nEquipping the Saints to live Purposeful lives\r\nEffecting change in the community', 'paypal.PNG', '2020-02-21 10:08:17'),
(2, '8', 7, 'Testing Page', 'Testing-Page', 'sfdsdfsdf', 'paypal.PNG', '2020-03-03 12:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `prayer_request`
--

CREATE TABLE `prayer_request` (
  `id` int(11) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `EmailAddress` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `streetaddress` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `PrayerFor` varchar(255) NOT NULL,
  `Prayer_msg` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prayer_request`
--

INSERT INTO `prayer_request` (`id`, `FirstName`, `LastName`, `EmailAddress`, `Phone`, `streetaddress`, `city`, `state`, `zip`, `dob`, `PrayerFor`, `Prayer_msg`, `created_date`) VALUES
(1, 'Tester', 'Demo', 'tettetr12@test.in', '9898989898', '', '', '', '', '', 'Need A Job', '', '2020-03-03 05:42:01'),
(5, 'Testing', 'Demo', 'testerdemo2@test.in', '1234567895', 'Mohali', 'Mohali', 'Select State', '160059', '1994-06-15', 'Need A Job', '', '2020-03-03 05:42:01'),
(6, 'Tetser', 'sdfsdf', 'test1developer198@gmail.com', '+919899898989898', 'Washington Street, DC, DC', 'New York', 'NY', '10001', '1999-02-10', 'Healing', '', '2020-03-03 05:42:01'),
(7, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '+919899898989898', 'Washington Street, DC, DC', 'New York', 'NY', '10001', '', 'Salvation', '', '2020-03-03 05:42:01'),
(8, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '9899898989898', 'Washington Street, DC, DC', 'New York', 'NY', '10001', '1998-02-04', 'Healing', 'This is a Prayer Message.', '2020-03-03 14:02:34');

-- --------------------------------------------------------

--
-- Table structure for table `sociaLIcons`
--

CREATE TABLE `sociaLIcons` (
  `id` int(11) NOT NULL,
  `facebookLink` varchar(255) NOT NULL,
  `instagramLink` varchar(255) NOT NULL,
  `youtubeLink` varchar(255) NOT NULL,
  `twitterLink` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sociaLIcons`
--

INSERT INTO `sociaLIcons` (`id`, `facebookLink`, `instagramLink`, `youtubeLink`, `twitterLink`, `created_at`, `updated_at`) VALUES
(1, 'https://www.facebook.com/RCCGSCC', 'https://www.instagram.com/rccg_scc/', 'https://www.youtube.com/channel/UCKodt0YtdTCsFP_1bFO5GtA?view_as=subscriber\"', 'https://twitter.com/RCCGSCC', '2020-02-20 16:01:28', '2020-02-20 10:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `subscriber`
--

CREATE TABLE `subscriber` (
  `id` int(11) NOT NULL,
  `subscriberName` varchar(255) NOT NULL,
  `subscriberEmail` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=>active,0=>inactive',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriber`
--

INSERT INTO `subscriber` (`id`, `subscriberName`, `subscriberEmail`, `status`, `created_at`, `updated_at`) VALUES
(32, 'subscriber', 'A@gmail.com', '0', '2020-02-25 19:37:59', '2020-02-25 14:07:59'),
(2, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-20 18:08:26', '2020-02-20 12:38:26'),
(3, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-20 18:11:41', '2020-02-20 12:41:41'),
(4, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-20 18:12:59', '2020-02-20 12:42:59'),
(5, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-20 18:15:28', '2020-02-20 12:45:28'),
(6, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-20 18:20:56', '2020-02-20 12:50:56'),
(7, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-20 18:22:29', '2020-02-20 12:52:29'),
(8, 'subscriber', 'humanpixel.univ@gmail.com', '0', '2020-02-20 18:27:48', '2020-02-20 12:57:48'),
(9, 'subscriber', 'humanpixel.univ@gmail.com', '0', '2020-02-20 18:29:30', '2020-02-20 12:59:30'),
(11, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-21 11:05:32', '2020-02-21 05:35:32'),
(56, 'subscriber', 'testd@gmail.com', '0', '2020-02-28 12:39:36', '2020-02-28 07:09:36'),
(30, 'subscriber', 'a@gmail.com', '0', '2020-02-25 18:42:53', '2020-02-25 13:12:53'),
(15, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-21 11:24:16', '2020-02-21 05:54:16'),
(16, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-21 11:25:54', '2020-02-21 05:55:54'),
(17, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-21 11:26:32', '2020-02-21 05:56:32'),
(18, 'subscriber', 'qa75741@gmail.com', '0', '2020-02-21 11:27:21', '2020-02-21 05:57:21'),
(20, 'subscriber', 'a@gmail.com', '0', '2020-02-25 18:16:56', '2020-02-25 12:46:56'),
(21, 'subscriber', 'k@gmail.com', '0', '2020-02-25 18:24:42', '2020-02-25 12:54:42'),
(22, 'subscriber', 'a@gmail.com', '0', '2020-02-25 18:25:27', '2020-02-25 12:55:27'),
(23, 'subscriber', 'a@gmail.com', '0', '2020-02-25 18:26:00', '2020-02-25 12:56:00'),
(24, 'subscriber', 'a@gmail.com', '0', '2020-02-25 18:26:03', '2020-02-25 12:56:03'),
(25, 'subscriber', 'a@gmail.com', '0', '2020-02-25 18:26:14', '2020-02-25 12:56:14'),
(26, 'subscriber', 'a@gmail.com', '0', '2020-02-25 18:27:32', '2020-02-25 12:57:32'),
(27, 'subscriber', 'aa@gmail.com', '0', '2020-02-25 18:35:13', '2020-02-25 13:05:13'),
(28, 'subscriber', 'NitishSir@gmail.com', '0', '2020-02-25 18:35:31', '2020-02-25 13:05:31'),
(44, 'subscriber', 'a@gmail.com', '0', '2020-02-25 20:03:24', '2020-02-25 14:33:24'),
(57, 'subscriber', 'addd@gmail.com', '0', '2020-02-28 17:12:40', '2020-02-28 11:42:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_role` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `video` varchar(255) DEFAULT NULL,
  `reagister_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `zipcode`, `email`, `username`, `user_role`, `password`, `video`, `reagister_date`) VALUES
(7, 'Shubham', '160055', 'shubhamtest12@test.in', 'shubham12@test.in', 'admin', '0192023a7bbd73250516f069df18b500', 'https://www.youtube.com/embed/81c6-16ZkIc', '2020-01-29 07:07:07'),
(9, 'Tetser sdfsdf', '10001', 'admin123@gmail.com', 'tango', '', '5548b66d9f1245fe7d5013b4de66d0ec', NULL, '2020-02-26 06:52:16'),
(10, 'eweb', '160055', 'eweb12@test.in', 'ewebone', 'subscriber', '25f9e794323b453885f5181f1b624d0b', NULL, '2020-02-26 07:04:50');

-- --------------------------------------------------------

--
-- Table structure for table `women_ministry`
--

CREATE TABLE `women_ministry` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `streetaddr` varchar(255) NOT NULL,
  `streetaddr2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `women_ministry`
--

INSERT INTO `women_ministry` (`id`, `fname`, `lname`, `email`, `phone`, `address`, `streetaddr`, `streetaddr2`, `city`, `zip_code`, `state`, `age`, `created_date`) VALUES
(1, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '9899898989898', 'mohali', 'Washington Street, DC, DC', 'DC', 'New York', '10001', 'New York', '2000-02-02', '2020-03-02 12:31:45'),
(2, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '9899898989898', 'Mohali Punjab', 'Washington Street, DC, DC', 'DC', 'New York', '10001', 'New York', '1995-02-08', '2020-03-02 12:32:08'),
(3, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '9899898989898', 'Mohali Punjab', 'Washington Street, DC, DC', 'DC', 'New York', '10001', 'New York', '1998-02-04', '2020-03-02 12:41:53'),
(4, 'D', 'FFR', 'FF@gmail.com', '8797978787', 'dd', 'ddd', 'ddd', 'zsss', '123456', 'California', '1993-02-09', '2020-03-02 12:43:40'),
(5, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '9899898989898', 'Mohali Punjab', 'Washington Street, DC, DC', 'DC', 'New York', '10001', 'New York', '1996-02-14', '2020-03-02 12:52:12'),
(6, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '9899898989898', 'Mohali Punjab', 'Washington Street, DC, DC', 'DC', 'New York', '10001', 'New York', '1989-02-15', '2020-03-02 12:52:33'),
(7, 'hhh', 'hhh', 'w@gmail.com', '3333333333', 'whitehead avenue', 'rrrrr', '44343eded', 'rrrr', '34567', 'Oklahoma', '1992-02-04', '2020-03-03 05:16:17'),
(8, 'Shalu', 'Kumari', 'shallu12@test.in', '9856321548', 'Mohali Punjab', 'Washington Street, DC, DC', 'DC', 'New York', '10001', 'New York', '2000-02-16', '2020-03-03 05:21:02'),
(9, 'Reena', 'Rana', 'reen12@test.in', '9856321458', 'Mohali Punjab', 'CHD', 'DC', 'Chad', '160055', 'Arizona', '1991-02-07', '2020-03-03 05:23:53'),
(10, 'Tetser', 'sdfsdf', 'admin123@gmail.com', '9899898989898', 'Mohali Punjab', 'Washington Street, DC, DC', 'DC', 'New York', '10001', 'New York', '1989-02-15', '2020-03-03 05:24:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ask_pastor_wale`
--
ALTER TABLE `ask_pastor_wale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `believers_class`
--
ALTER TABLE `believers_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `circle_registeration`
--
ALTER TABLE `circle_registeration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footerBottom`
--
ALTER TABLE `footerBottom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeSliderImages`
--
ALTER TABLE `homeSliderImages`
  ADD PRIMARY KEY (`imageId`);

--
-- Indexes for table `Invite`
--
ALTER TABLE `Invite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_application`
--
ALTER TABLE `job_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kids`
--
ALTER TABLE `kids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manthegate`
--
ALTER TABLE `manthegate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `office_scheduler`
--
ALTER TABLE `office_scheduler`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `onlineVisitors`
--
ALTER TABLE `onlineVisitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_donations`
--
ALTER TABLE `online_donations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prayer_request`
--
ALTER TABLE `prayer_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sociaLIcons`
--
ALTER TABLE `sociaLIcons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriber`
--
ALTER TABLE `subscriber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `women_ministry`
--
ALTER TABLE `women_ministry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ask_pastor_wale`
--
ALTER TABLE `ask_pastor_wale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `believers_class`
--
ALTER TABLE `believers_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `circle_registeration`
--
ALTER TABLE `circle_registeration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `footerBottom`
--
ALTER TABLE `footerBottom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `homeSliderImages`
--
ALTER TABLE `homeSliderImages`
  MODIFY `imageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `Invite`
--
ALTER TABLE `Invite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_application`
--
ALTER TABLE `job_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kids`
--
ALTER TABLE `kids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manthegate`
--
ALTER TABLE `manthegate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `office_scheduler`
--
ALTER TABLE `office_scheduler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `onlineVisitors`
--
ALTER TABLE `onlineVisitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `online_donations`
--
ALTER TABLE `online_donations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `prayer_request`
--
ALTER TABLE `prayer_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sociaLIcons`
--
ALTER TABLE `sociaLIcons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscriber`
--
ALTER TABLE `subscriber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `women_ministry`
--
ALTER TABLE `women_ministry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
